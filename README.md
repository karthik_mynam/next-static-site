This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## Build & run Docker

```$xslt
$ docker build -t next-static-site .
$ docker run -d -p 3000:3000 next-static-site
```

To delete all containers and images - 

```$xslt
$ docker rm -vf $(docker ps -aq); docker rmi -f $(docker images -aq)
```

## Push to Docker Hub

```$xslt
$ docker login --username=karthikmynam
Password: 
WARNING! Your password will be stored unencrypted in /home/karthik/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
$ docker images
REPOSITORY         TAG         IMAGE ID       CREATED       SIZE
next-static-site   latest      91b41da47ae8   2 days ago    313MB
$ docker tag next-static-site karthikmynam/next-static-site:v1
$ docker images
REPOSITORY                      TAG         IMAGE ID       CREATED       SIZE
karthikmynam/next-static-site   v1          91b41da47ae8   2 days ago    313MB
$ docker push karthikmynam/next-static-site:v1
```

## References

 - https://www.coderrocketfuel.com/article/how-to-deploy-a-next-js-website-to-a-digital-ocean-server
 - https://blog.zack.computer/docker-containers-nodejs-nextjs
 
  ##### Others
  - https://hashnode.jamesperkins.dev/how-to-reload-an-ssr-page
  - https://medusajs.hashnode.dev/create-your-ecommerce-store-with-nextjs-in-minutes
  - https://medusajs.com/