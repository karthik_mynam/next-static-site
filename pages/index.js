import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import logo from '../public/images/robot-p.png';
import robotPic from '../public/images/image-px.png';
import {useEffect, useState} from "react";
import axios from "axios";

// References: https://www.digitalocean.com/community/tutorials/how-to-build-a-rate-limiter-with-node-js-on-app-platform

const url = "https://api.randaevu.com/";


export default function Home({ip}) {

    const [test, setTest] = useState(undefined);
    const [name, setName] = useState(undefined);

    useEffect(() => {

        async function fetchData() {
            // You can await here
            console.log("Make backend call");

            const res = await getTest();
            if (res)
                setTest(res);

            const out = await getEntry();
            if (out)
                setName(out["name"]);
            // ...
        }
        fetchData();

    },[]);

    const getTest = async () => {

        console.log("calling backend - test");
        const response = await axios.get(url + 'test');

        if (response && response.data) {
            console.log("Received: " + JSON.stringify(response.data));
            return response.data;
        }
        else
            console.log("Received no data");

        console.log("done calling backend");
        return undefined;
    }

    const getEntry = async () => {

      console.log("calling backend - dbtest");
      const response = await axios.get(url + 'dbtest');

      if (response && response.data) {
          console.log("Received: " + JSON.stringify(response.data));
          return response.data;
      }
      else
          console.log("Received no data");

      console.log("done calling backend");
      return undefined;
  }

  return (
      <div className="bg-gray-50 py-10 grid grid-rows-2 justify-items-center items-center h-screen px-20">
          <div className="bg-gray-70 w-full px-10 border-2 rounded-md">
              <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-24 lg:px-8 lg:flex lg:items-center lg:justify-between">

                  <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
                      <div className="inline-flex rounded-md shadow">
                          <div
                              className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white hover:bg-gray-300"
                          >
                              <Image
                                  src={logo}
                                  alt="Robot looking up"
                                  width={50}
                                  height={50}
                                  quality="90"
                                  objectFit='contain'
                              />
                          </div>
                      </div>
                  </div>

                  <h2 className="text-3xl font-extrabold tracking-tight text-gray-900 md:text-4xl">
                      <span className="block">Ready to dive in?</span>
                      <span className="block text-indigo-600">This is site Main (m-db-7)</span>
                      <span className="block text-[20px] text-gray-600">test: {test}</span>
                      <span className="block text-[20px] text-gray-600">name: {name}</span>
                      <span className="block text-[20px] text-gray-600">client: {ip}</span>
                  </h2>

              </div>
          </div>
          <div style={{width: '100%', height: '100%', position: 'relative'}}>
              {/* https://uploadcare.com/blog/next-js-image-optimization/ */}
              {/* loading="eager" removing makes it lazy load (which is default) */}
              <Image
                  src={robotPic}
                  alt="Robot looking up"
                  width={600}
                  height={450}
                  quality="90"
                  layout="responsive"
                  objectFit='contain'
              />
          </div>

      </div>

          )
  }


export const getServerSideProps = async ({ req }) => {
    const forwarded = req.headers['x-forwarded-for'];

    const ip = typeof forwarded === 'string' ? forwarded.split(/, /)[0] : req.socket.remoteAddress;

    console.log(JSON.stringify(req.headers));
    console.log("f: " + forwarded);
    console.log("r: " + req.headers['x-real-ip']);
    console.log(ip);

    return {
        props: { ip },
    };
};